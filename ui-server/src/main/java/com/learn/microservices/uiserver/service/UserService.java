package com.learn.microservices.uiserver.service;

import com.learn.microservices.uiserver.model.LoginVM;
import com.learn.microservices.uiserver.model.Message;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

@Service
public class UserService
{
    @Autowired
    private RestTemplate restTemplate;

    public boolean login(LoginVM loginVM)
    {
        String url = "http://zuul-proxy-server/usermanagement/login";
        ResponseEntity<Message> messageResponseEntity = restTemplate.postForEntity(url,loginVM,Message.class);
        if (messageResponseEntity.getStatusCode() == HttpStatus.OK)
        {
             Message message1 = messageResponseEntity.getBody();
             if (message1.getErrorCode() == 1020)
                 return true;
             else
                 return false;
        }
        else
            throw new RuntimeException();
    }
}
