package com.learn.microservices.uiserver.controller;

import com.learn.microservices.uiserver.model.Message;
import com.learn.microservices.uiserver.model.Order;
import com.learn.microservices.uiserver.model.OrderList;
import com.learn.microservices.uiserver.model.Product;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.client.RestTemplate;

import javax.servlet.http.HttpSession;
import java.util.ArrayList;
import java.util.List;

@Controller
public class OrderController
{
    @Autowired
    private RestTemplate restTemplate;

    @GetMapping("/order")
    public String placeOrder(@RequestParam("productcode")String productcode, Model model,HttpSession session)
    {
        String userid=(String) session.getAttribute("userid");
        String url= "http://zuul-proxy-server/order/place-order";
        ResponseEntity<Message> messageResponseEntity = restTemplate.postForEntity(url,new Order(userid,productcode),Message.class);

        if (messageResponseEntity.getStatusCode() == HttpStatus.OK)
        {
            Message message =messageResponseEntity.getBody();
            if (message.getErrorCode() == 1030)
            {
               model.addAttribute("msg",message.getMessage());
            }
            else {
                model.addAttribute("msg", message.getMessage());
            }
            return "home";
        }
        else
        {
            model.addAttribute("msg","something went wrong while placing the order");
            return "home";
        }

    }


    @GetMapping("/myorder")
    public String fetchMyOrder(Model model,HttpSession session)
    {
        String userid = (String)session.getAttribute("userid");
        String url ="http://zuul-proxy-server/order/find-order?userid="+userid;

        ResponseEntity<OrderList> orderListResponseEntity=restTemplate.getForEntity(url, OrderList.class,new Object());

        if (orderListResponseEntity.getStatusCode() == HttpStatus.OK)
        {
            List<Product> products = new ArrayList<>();
            OrderList orderList = orderListResponseEntity.getBody();

            for (Order o : orderList.getOrderList())
            {
                String url_product="http://zuul-proxy-server/product-stock/product-by-code?productcode=";
              url_product=url_product+o.getProductid();
              ResponseEntity<Product> productResponseEntity=restTemplate.getForEntity(url_product, Product.class,new Object());
              if (productResponseEntity.getStatusCode() == HttpStatus.OK)
              {
                  products.add(productResponseEntity.getBody());
              }
            }

         model.addAttribute("productlist",products);
            return "myorder";
        }

        else
        {
            model.addAttribute("msg","Unable to fetch order list");
            return "home";
        }



    }
}
