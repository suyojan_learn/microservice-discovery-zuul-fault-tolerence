package com.learn.microservices.uiserver.controller;

import com.learn.microservices.uiserver.model.*;
import com.learn.microservices.uiserver.service.ProductService;
import com.learn.microservices.uiserver.service.UserService;
import com.netflix.discovery.converters.Auto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.client.ServiceInstance;
import org.springframework.cloud.client.discovery.DiscoveryClient;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.client.RestTemplate;

import javax.servlet.http.HttpSession;
import java.net.MalformedURLException;
import java.util.List;

@Controller
public class UserController
{
  @Autowired
  private RestTemplate restTemplate;

  @Autowired
  private UserService userService;

  @Autowired
  private ProductService productService;

  @GetMapping("/register")
  public String registrationPage(Model model)
  {
      model.addAttribute("user",new User());
      return "registration";
  }

  @PostMapping("/register")
  public String registerUser(@ModelAttribute("user")User user,Model model) throws MalformedURLException
  {
    String url = "http://zuul-proxy-server/usermanagement/register";
    ResponseEntity<Message> message=restTemplate.postForEntity(url,user, Message.class);
    if (message.getStatusCode() == HttpStatus.OK)
    {
       Message message1 = message.getBody();
       model.addAttribute("msg",message1.getMessage());
    }
    else
    {
        model.addAttribute("msg","there are some problem in registration.. kindly try after some times");
    }
    return "registration";
  }

  @GetMapping("/login")
  public String loginPage(Model model)
  {
      model.addAttribute("loginvm",new LoginVM());
      return "login";
  }

  @PostMapping("/login")
  public String login(@ModelAttribute("lginvm")LoginVM loginVM, Model model, HttpSession session) throws MalformedURLException
  {

      if (userService.login(loginVM))
      {
          List<Product> productList = productService.getProductList();
              if (productList != null || !productList.isEmpty())
              {
                  session.setAttribute("userid",loginVM.getUserName());
                  model.addAttribute("productlist",productList);
              }
              return "inbox";
      }
          else
          {
              model.addAttribute("msg","Login failed");
              model.addAttribute("loginvm",new LoginVM());
              return "login";
          }

  }


}

