package com.learn.microservices.uiserver.service;


import com.learn.microservices.uiserver.model.Product;
import com.learn.microservices.uiserver.model.ProductVM;
import com.netflix.hystrix.contrib.javanica.annotation.HystrixCommand;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.util.List;

@Service
public class ProductService
{
    @Autowired
    private RestTemplate restTemplate;


    @HystrixCommand(fallbackMethod = "getProductListFromBackup")
    public List<Product> getProductList()
    {
        System.out.println("----------->MAIN<------------------");
        try {
            String stock_url="http://zuul-proxy-server/product-stock/product-list";
            ResponseEntity<ProductVM> productsProductVMResponseEntity = restTemplate.getForEntity(stock_url, ProductVM.class,new Object());
            if (productsProductVMResponseEntity.getStatusCode() == HttpStatus.OK)
            {
                return productsProductVMResponseEntity.getBody().getProductList();
            }
            else
                throw new RuntimeException();
        }
        catch (Exception ex)
        {
          throw new RuntimeException();
        }


    }

    public List<Product> getProductListFromBackup()
    {
        System.out.println("---------------->FALLBACK<------------------");
        String stock_url="http://zuul-proxy-server/product-stock-backup/product-list";
        ResponseEntity<ProductVM> productsProductVMResponseEntity = restTemplate.getForEntity(stock_url, ProductVM.class,new Object());
        return productsProductVMResponseEntity.getBody().getProductList();
    }
}
