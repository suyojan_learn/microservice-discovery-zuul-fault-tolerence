package com.learn.microservice.usermanagement.web.rest;


import com.learn.microservice.usermanagement.entity.MSUser;
import com.learn.microservice.usermanagement.model.LoginVM;
import com.learn.microservice.usermanagement.model.MSMessage;
import com.learn.microservice.usermanagement.service.UserManagementService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/usermanagement")
public class UserManagementController
{

    @Autowired
    private UserManagementService userManagementService;

    @GetMapping("/hello")
    public String greet()
    {
        return "Hello World";
    }


    @PostMapping("/register")
    public ResponseEntity<MSMessage> register(@RequestBody MSUser msUser)
    {
       if (userManagementService.registerUser(msUser))
       {
           return ResponseEntity.ok(new MSMessage("User registered successfully",1010,"Success"));
       }
       else
           return ResponseEntity.ok(new MSMessage("Registration Fail",2010,"Failed"));
    }

    @PostMapping("/login")
    public ResponseEntity<MSMessage> login(@RequestBody LoginVM loginVM)
    {
        MSMessage msMessage= new MSMessage();
      if(userManagementService.loginUser(loginVM))
      {
          msMessage.setErrorCode(1020);
          msMessage.setMessage("Login Success");
          msMessage.setTitle("Success");
      }
      else
      {
          msMessage.setErrorCode(2020);
          msMessage.setMessage("Login Failed");
          msMessage.setTitle("Fail");
      }

      return ResponseEntity.ok(msMessage);
    }



}
