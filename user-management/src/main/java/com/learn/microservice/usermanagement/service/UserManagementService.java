package com.learn.microservice.usermanagement.service;


import com.learn.microservice.usermanagement.entity.MSUser;
import com.learn.microservice.usermanagement.model.LoginVM;
import com.learn.microservice.usermanagement.repository.MSUserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

@Service
public class UserManagementService
{
    @Autowired
    private MSUserRepository repository;

    @Transactional
    public boolean registerUser(MSUser user)
    {
        MSUser user1=repository.saveAndFlush(user);
        return user1.getUserid() != 0;
    }


    public boolean loginUser(LoginVM loginVM)
    {
        Optional<MSUser> user=repository.findByUsernameAndPassword(loginVM.getUserName(),loginVM.getPassword());
        return user.isPresent();
    }
}
