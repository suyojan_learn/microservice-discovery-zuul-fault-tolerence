package com.learn.microservice.usermanagement.repository;


import com.learn.microservice.usermanagement.entity.MSUser;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface MSUserRepository extends JpaRepository<MSUser,Integer> {
    Optional<MSUser> findByUsernameAndPassword(String userName, String password);
}
