package com.learn.microservice.productstockbackup.service;


import com.learn.microservice.productstockbackup.entity.Product;
import com.learn.microservice.productstockbackup.model.ProductDTO;
import com.learn.microservice.productstockbackup.repository.ProductRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ProductService
{
    @Autowired
    private ProductRepository productRepository;
    public boolean addProduct(Product product)
    {
        Product savedProduct=productRepository.saveAndFlush(product);
        return savedProduct.getProductId() != 0;
    }

    public List<Product> productList()
    {
        return productRepository.findAll();
    }

    public ProductDTO getProductByCode(String code)
    {
        Product product=productRepository.findByProductCode(code);
        ProductDTO productVM=new ProductDTO();
        productVM.setProductCode(product.getProductCode());
        productVM.setProductName(product.getProductName());
        productVM.setProductPrice(product.getProductPrice());
        return productVM;

    }
}
