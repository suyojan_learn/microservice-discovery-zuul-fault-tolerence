package com.learn.microservice.productstockbackup.repository;


import com.learn.microservice.productstockbackup.entity.Product;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ProductRepository extends JpaRepository<Product,Long>
{
    Product findByProductCode(String code);
}
