package com.learn.microservice.productstockbackup.controller;


import com.learn.microservice.productstockbackup.entity.Product;
import com.learn.microservice.productstockbackup.model.MSMessage;
import com.learn.microservice.productstockbackup.model.ProductDTO;
import com.learn.microservice.productstockbackup.model.ProductVM;
import com.learn.microservice.productstockbackup.service.ProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/product-stock")
public class ProductController
{

    @Autowired
    private ProductService productService;

    @PostMapping("/product-add")
    public ResponseEntity<MSMessage> addProduct(@RequestBody Product product)
    {
        MSMessage msMessage=null;
      if (productService.addProduct(product))
      {
           msMessage= new MSMessage("Product added successfully",1030,"Success");
      }
      else
      {
          msMessage=new MSMessage("Product not added",2030,"Failed");
      }
      return ResponseEntity.ok(msMessage);

    }

    @GetMapping("/product-list")
    public ResponseEntity<ProductVM> productList()
    {
      return ResponseEntity.ok(new ProductVM(productService.productList()));
    }


    @GetMapping("/product-by-code")
    public ResponseEntity<ProductDTO> productByCode(@RequestParam("productcode")String productcode)
    {
       return ResponseEntity.ok(productService.getProductByCode(productcode));
    }
}
