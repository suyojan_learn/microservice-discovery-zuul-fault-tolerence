package com.learn.microservice.productstockbackup;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ProductStockBackupApplication {

    public static void main(String[] args) {
        SpringApplication.run(ProductStockBackupApplication.class, args);
    }

}
