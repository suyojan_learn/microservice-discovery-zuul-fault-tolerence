package com.learn.microservice.productstockbackup.model;

import com.learn.microservice.productstockbackup.entity.Product;

import java.util.List;

public class ProductVM
{
    private List<Product> productList;


    public ProductVM() {
    }

    public ProductVM(List<Product> productList) {
        this.productList = productList;
    }

    public List<Product> getProductList() {
        return productList;
    }

    public void setProductList(List<Product> productList) {
        this.productList = productList;
    }
}
